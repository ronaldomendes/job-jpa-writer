package com.cursospring.batch.jobjpawriter.writer;

import com.cursospring.batch.jobjpawriter.model.Employee;
import com.cursospring.batch.jobjpawriter.repository.EmployeeRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
@Slf4j
public class EmployeeDBWriter implements ItemWriter<Employee> {

    private final EmployeeRepository repository;

    @Override
    public void write(List<? extends Employee> employees) throws Exception {
        repository.saveAll(employees);
        log.info("{} employees saved in database.", employees.size());
    }
}
