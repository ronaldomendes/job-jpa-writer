package com.cursospring.batch.jobjpawriter;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan(basePackages = {"com.cursospring.batch.jobjpawriter"})
public class JobJpaWriterApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobJpaWriterApplication.class, args);
    }

}
