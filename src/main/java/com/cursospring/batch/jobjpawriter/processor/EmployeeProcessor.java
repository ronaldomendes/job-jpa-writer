package com.cursospring.batch.jobjpawriter.processor;

import com.cursospring.batch.jobjpawriter.dto.EmployeeDTO;
import com.cursospring.batch.jobjpawriter.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class EmployeeProcessor implements ItemProcessor<EmployeeDTO, Employee> {

    @Override
    public Employee process(EmployeeDTO dto) throws Exception {
        Employee employee = new Employee();
        employee.setEmployeeId(dto.getEmployeeId());
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        employee.setAge(dto.getAge());
        employee.setUpdatedIn(new Date());
        log.info("Inside processor. Employee: {}", employee.toString());
        return employee;
    }
}