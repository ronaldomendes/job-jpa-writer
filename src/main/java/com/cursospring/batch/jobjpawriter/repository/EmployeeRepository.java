package com.cursospring.batch.jobjpawriter.repository;

import com.cursospring.batch.jobjpawriter.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String> {
}
