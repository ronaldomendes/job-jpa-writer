package com.cursospring.batch.jobjpawriter.model;

import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "tb_employee")
@DynamicInsert
@DynamicUpdate
public class Employee {

    @Id
    private String employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private int age;
    private Date updatedIn; // atributo adicionado para facilitar a visualização das atualizações no H2
}
