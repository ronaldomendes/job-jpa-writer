# Spring Batch: Job CSV Reader with JPA Writer

In this example you will learn how to migrate some CSV data into any database using JPA resources with Spring Batch architecture.